# Learning general software interview problem-solving skills

Just one of the things I'm learning. https://github.com/hchiam/learning

## General strategies and mnemonics

[AEIOU](https://hchiam.blogspot.com/2019/03/programming-problem-solving-mnemonic.html)

[Visual](https://hchiam.blogspot.com/2017/02/10-programming-data-structures-and-algorithms-in-pictures.html) [reminders](https://hchiam.blogspot.com/2019/03/random-visual-mnemonics-for-programming.html)

[Connections](https://hchiam.blogspot.com/2016/11/learning-faster-and-memory-structures.html)

_**Don't get stuck:**_ In your own words. Aloud. Draw. Examples. Manually. Pseudocode. Brute force first. `Ot(?)` and `Os(?)`. Best conceivable runtime. Patterns. Heuristics. Base case and build. Try with just one item first (even for an iterative solution). See if sorting helps you find a pattern. Run through the data structure "alphabet" (hash table, etc.). [Put some key CtCI takeaways into your mind palace](https://app.memrise.com/course/6044034/abridged-ctci-mp-71/).

## Practice

[CtCI (Cracking the Coding Interview)](https://www.crackingthecodinginterview.com) and [approaches](https://www.crackingthecodinginterview.com/uploads/6/5/2/8/6528028/cracking_the_coding_skills_-_v6.pdf) and [behavioral](https://www.crackingthecodinginterview.com/uploads/6/5/2/8/6528028/cracking_the_soft_skills_-_v6.pdf).

[LeetCode](https://leetcode.com): practice/explanations of [fundamentals](https://leetcode.com/explore/learn) and [simulated assessments/interviews](https://leetcode.com/assessment). Otherwise filter by rating for quality questions, or find the topic lists curated by LeetCode staff, which also have official explanations and solutions.

[CPH (Competitive Programmer's Handbook)](https://github.com/pllk/cphb).

[AlgoExpert](https://www.algoexpert.io): less questions, but more focus. No need to filter questions for quality questions/solutions/explanations like you would for LeetCode. Curated content, explanation videos, and tips for behavioural, etc. AlgoExpert = deeper into conceptual consolidation; LeetCode = wider practice.

[Clean code and code review tips](https://github.com/hchiam/random-code-tips).

## Random fun

[Roots](https://hchiam.blogspot.com/2017/10/mentally-finding-roots-and-squares-or.html)

[Binary](https://hchiam.blogspot.com/2015/09/how-to-quickly-convert-binary-to-decimal.html)

[Mental](https://hchiam.blogspot.com/2017/08/google-voice-mental-temperature-converter.html) [temperature](https://hchiam.blogspot.com/2016/09/how-to-convert-temperature-quickly-in.html) [conversion](https://hchiam.blogspot.com/2015/12/how-to-convert-temperature-f-c-quickly.html)

[Stop spinning your wheels at work](https://willmurphyscode.net/2016/04/06/learning-hack-recognize-and-interrupt-wheel-spinning): Can't figure out what's broken?
**Root cause tree**. Suspect **recent** changes. **Batch questions** for hard-to-check possible causes.
